package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{

    int max_size = 20;
    ArrayList<FridgeItem> inFridge = new ArrayList<FridgeItem>();
    ArrayList<FridgeItem> expiredFood = new ArrayList<FridgeItem>();


    @Override
    public int totalSize(){
        return max_size;
    }

    @Override
    public int nItemsInFridge() {
        return inFridge.size();
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge() < totalSize()){
            inFridge.add(item);
            return true;}
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (inFridge.contains(item)){
            inFridge.remove(item);
            return;
        }     
        else {
            throw new NoSuchElementException();}
    }

    @Override
    public void emptyFridge() {
           inFridge.clear();
           return;
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        for (int i=0; i<nItemsInFridge(); i++){
            if (inFridge.get(i).hasExpired()){
                expiredFood.add(inFridge.get(i));
                takeOut(inFridge.get(i));
                i--;
            }
        }
        return expiredFood;
    }
}
